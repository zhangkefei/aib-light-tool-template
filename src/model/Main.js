import { ProgramError, ProgramMessage, ProgramWarning } from "./ProgramMessage";
import { ResultTextMessage } from "./ResultTextMessage";
import { ResultFileMessage } from "./ResultFileMessage";
import { SystemMessage } from "./SystemMessage";
import InteractInputMode from "../assets/js/InteractInputMode";
import { detect } from "detect-browser";

class Utils {
    static sleep(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, ms);
        });
    }
}

export class Main {
    constructor({ messages, interaction }) {
        this._messages = messages
        this._interaction = interaction
        this._alert = {
            resolve: undefined
        }
        this._confirm = {
            resolve: undefined,
            reject: undefined
        }
        this._prompt = {
            resolve: undefined
        }
        this._evaluate = {
            resolve: undefined,
            reject: undefined
        }
        this.Utils = Utils
    }

    get messages() {
        return this._messages
    }

    get interaction() {
        return this._interaction
    }

    message(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m);
    }
    warning(msg) {
        const m = new ProgramWarning(msg)
        this.messages.push(m)
    }
    error(msg) {
        const m = new ProgramError(msg)
        this.messages.push(m)
    }
    systemMessage(msg) {
        const m = new SystemMessage(msg)
        this.messages.push(m)
    }
    alert(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m)
        this.interaction.mode = InteractInputMode.ALERT
        this.interaction.message = msg
        return new Promise((resolve, reject) => {
            this._alert.resolve = () => {
                resolve()
                this._alert.resolve = undefined
            }
        })
    }
    alertConfirm() {
        this._alert.resolve && this._alert.resolve()
        this.resetInteraction()
    }
    confirm(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m)
        this.interaction.mode = InteractInputMode.CONFIRM
        this.interaction.message = msg
        return new Promise((resolve) => {
            this._confirm.resolve = () => {
                resolve(true)
                this._confirm.resolve = undefined
                this._confirm.reject = undefined
            }
            this._confirm.reject = () => {
                resolve(false)
                this._confirm.resolve = undefined
                this._confirm.reject = undefined
            }
        })
    }
    confirmYes() {
        this._confirm.resolve && this._confirm.resolve()
        this.resetInteraction()
    }
    confirmNo() {
        this._confirm.reject && this._confirm.reject()
        this.resetInteraction()
    }
    prompt(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m)
        this.interaction.mode = InteractInputMode.PROMPT
        this.interaction.message = msg
        return new Promise((resolve, reject) => {
            this._prompt.resolve = (data) => {
                resolve(data)
                this._prompt.resolve = undefined
            }
        })
    }
    promptResolve(data) {
        this._prompt.resolve && this._prompt.resolve(data)
        this.resetInteraction()
    }
    file(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m)
        this.interaction.mode = InteractInputMode.FILE
        this.interaction.message = msg
        return new Promise((resolve, reject) => {
            this._prompt.resolve = (data) => {
                resolve(data)
                this._prompt.resolve = undefined
            }
        })
    }
    fileResolve(file) {
        this._prompt.resolve && this._prompt.resolve(file)
        this.resetInteraction()
    }
    dirHandle(msg) {
        const m = new ProgramMessage(msg)
        this.messages.push(m)
        this.interaction.mode = InteractInputMode.DIR_HANDLE
        this.interaction.message = msg
        return new Promise((resolve, reject) => {
            this._prompt.resolve = (data) => {
                resolve(data)
                this._prompt.resolve = undefined
            }
        })
    }
    dirHandleResolve(dirHandle) {
        this._prompt.resolve && this._prompt.resolve(dirHandle)
        this.resetInteraction()
    }
    evaluate() {
        this.interaction.mode = InteractInputMode.EVALUATE
        return new Promise((resolve, reject) => {
            this._evaluate.resolve = (evaluateLevel) => {
                resolve(evaluateLevel)
                this._evaluate.resolve = undefined
            }
            this._evaluate.reject = () => {
                reject()
                this._evaluate.reject = undefined
            }
        })
    }
    evaluateCancel() {
        this._evaluate.reject && this._evaluate.reject()
        this.resetInteraction()
    }
    evaluateSubmit(level) {
        this._evaluate.resolve && this._evaluate.resolve(level)
        this.resetInteraction()
    }
    processing() {
        const msg = "处理中..."
        this.interaction.mode = InteractInputMode.PROCESSING
        const m = new ProgramMessage(msg)
        this.messages.push(m);
        return m;
    }
    complete(processingMessage) {
        this.resetInteraction()
        const index = this.messages.findIndex(i => i.id === processingMessage.id);
        if (index > -1) {
            this.messages.splice(index, 1);
        }
    }
    result(data) {
        const typeString = Object.prototype.toString.call(data)
        let m
        switch (typeString) {
            case '[object String]':
                m = new ResultTextMessage(data)
                break;
            case '[object File]':
                m = new ResultFileMessage(data)
            default:
                break;
        }
        this.messages.push(m);
    }
    resetInteraction() {
        this.interaction.disabled = false
        this.interaction.message = '请输入你想做的事...'
        this.interaction.mode = InteractInputMode.INPUT
    }
    platform() {
        const worker = typeof document === 'undefined'
        const platform = {...detect(), worker}
        return platform
    }
}