import { Message } from "./Message";
import MessageOrigin from "../assets/js/MessageOrigin";
import MessageType from "../assets/js/MessageType";
export class ProgramMessage extends Message {
    constructor(msg) {
        super(msg)
        this.from = MessageOrigin.PROGRAM
    }
}

export class ProgramError extends ProgramMessage {
    constructor(msg) {
        super(msg)
        this.type = MessageType.ERROR
    }
}

export class ProgramWarning extends ProgramMessage {
    constructor(msg) {
        super(msg)
        this.type = MessageType.WARNING
    }
}