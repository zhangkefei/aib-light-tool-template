import { Message } from "./Message";
import MessageOrigin from "../assets/js/MessageOrigin";

export class SystemMessage extends Message {
    constructor(msg) {
        super(msg)
        this.from = MessageOrigin.SYSTEM
    }
}