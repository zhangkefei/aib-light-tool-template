import { Message } from "./Message";
import MessageOrigin from "../assets/js/MessageOrigin";

export class ResultMessage extends Message {
    constructor(msg) {
        super(msg)
        this.from = MessageOrigin.RESULT
    }
}