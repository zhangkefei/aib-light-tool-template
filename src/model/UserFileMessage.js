import { UserMessage } from "./UserMessage";

export class UserFileMessage extends UserMessage {
    constructor(file) {
        super(file.name)
        this._file = file
    }

    get size() {
        return this._file.size
    }
}