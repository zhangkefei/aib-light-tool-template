export default {
    USER: 'USER',
    PROGRAM: 'PROGRAM',
    RESULT: 'RESULT',
    SYSTEM: 'SYSTEM'
}