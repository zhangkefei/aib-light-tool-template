import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import '@shoelace-style/shoelace/dist/themes/light.css';
import { setBasePath } from '@shoelace-style/shoelace/dist/utilities/base-path';

setBasePath('/node_modules/@shoelace-style/shoelace/dist/');

import "@shoelace-style/shoelace/dist/components/icon/icon.js";
import "@shoelace-style/shoelace/dist/components/icon-button/icon-button.js";

console.log(`当前运行模式：${import.meta.env.MODE}`);

createApp(App).mount('#app')
