import md5 from "md5";

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

export default async (str) => {
    try {
        const confirm = await $.confirm('这个开发模板附带了一段示例交互，要看看吗？')
        if (confirm) {
            $.message('这是一条普通消息')
            $.message('检测平台: ' + JSON.stringify(await $.platform()))
            $.warning('这是一条警告消息')
            $.warning('这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息这是一条超长警告消息')
            $.error('这是一条错误消息')
            await $.alert('这是一条阻塞警告消息')
            const someText = await $.prompt('请输入一段测试文本，我将回显它')
            $.message('你刚才输入的文本是：' + someText)
            const testFile = await $.file('请选择一个文件， 我将告诉你它的大小')
            $.message('这个文件的大小是：' + testFile.size + ' Byte')
            const dirHandle = await $.dirHandle('请选择一个目录')
            const dirFiles = [];
            for await (const key of dirHandle.keys()) {
                dirFiles.push(key);
            }
            $.message(`${dirHandle.name}目录下的文件有：${dirFiles.join('，')}`)
            $.message('下面的结果是由 $.result 显示的')
            $.result(md5(str))
            $.message('示例交互结束')
            $.message('下面是md5加密结果')
            const p = await $.processing()
            await wait(3000)
            await $.complete(p)
        } else {
            $.message('那好吧，直接给你md5加密结果')
        }
        return md5(str)
    } catch (error) {
        $.error(error.message)
    }
}